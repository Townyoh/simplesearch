# SimpleSearch

**Problem**

This program should read all .txt file in given directory and then give a command prompt at which interactive searches can be performed.  
The search should take the words given on the prompt and return a list of the top 10 (maximum) matching filenames in rank order, giving the rank score against each match.

**Exemple**

Consider this 3 files:  
*  First.txt => a b c d e f  
*  Second.txt => a b c  
*  Third.txt => a b c d  

And then this command line:
*  search> a d e f

We should have:
*  First.txt:100%
*  Third.txt:50%
*  Second.txt:25%

**My solution**

You will find the exe in the zip file: SimpleSearch.exe  
Should be launch: SimpleSearch.exe DirectoryToTestFile

**Ranking system & some choises**

For the ranking system I decided to take an easy system:
*  number of unique input word find in file (nbWordFindInFile)
*  number of unique word in input (nbUniqueWordInput)
*  Pourcentage = 100 * nbWordFindInFile / nbUniqueWordInput

I also decided to take duplicate words in file as unique as same as input word.  
Our ranking system and took each duplicate word as unique work fine together

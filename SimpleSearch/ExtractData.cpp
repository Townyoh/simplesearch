#include <iso646.h>

#include "ExtractData.h"

ExtractData::ExtractData(const std::string& file)
  : filePath(file)
{
  stream.open(filePath);
  if (not stream.is_open())
  {
    throw std::string("Unable to open file.");
  }
}

ExtractData::~ExtractData()
{
  stream.close();
}

bool ExtractData::getNextInformation(std::string& info)
{
  if (this->stream.eof())
    return false;

  std::getline(this->stream, info);

  return true;
}

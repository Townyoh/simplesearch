#pragma once

#include <fstream>
#include <string>

class ExtractData
{
  std::string filePath;
  std::ifstream stream;

public:
  ExtractData(const std::string& file);
  ~ExtractData();

  bool getNextInformation(std::string& info);

};

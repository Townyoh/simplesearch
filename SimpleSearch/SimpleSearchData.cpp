#include <filesystem>
#include <sstream>
#include <iso646.h>

#include "SimpleSearchData.h"
#include "ExtractData.h"

bool SimpleSearchData::saveFile(const std::string& path)
{
  if (not this->isCorrectFilePath(path))
    return false;

  try
  {
    ExtractData extractor(path);
    auto filename = std::filesystem::path(path).filename().string();
    auto line = std::string();
    auto counter = 0;

    while (extractor.getNextInformation(line))
    {
      if (line.empty())
        continue;

      counter += this->saveLine(line, filename);
    }

    this->nbWordByFile.insert(std::make_pair(filename, counter));
  }
  catch (const std::string& ex)
  {
    throw ex;
  }
  catch (...)
  {
    throw;
  }

  return true;
}

int SimpleSearchData::howManyWordsInFile(const std::string& file) const
{
  auto elem = this->nbWordByFile.find(file);
  auto nbWords = -1;

  if (elem != this->nbWordByFile.end())
    nbWords = elem->second;

  return nbWords;
}

std::vector<std::string> SimpleSearchData::getListOfFileThatIncludeWord(const std::string& word) const
{
  auto elem = this->wordInformation.find(word);
  auto fileList = std::vector<std::string>();

  if (elem != this->wordInformation.end())
    fileList.insert(fileList.end(), elem->second.begin(), elem->second.end());

  return fileList;
}

bool SimpleSearchData::isCorrectFilePath(const std::string& path) const
{
  if (path.size() > 4 && std::filesystem::path(path).extension() == ".txt")
    return true;

  return false;
}

// We read file line by line, we don't want to map all the file into one string
// We also count the number of word that we met
// We count word in arbitraty way; even if a word appear multiple time that count only for one
int SimpleSearchData::saveLine(const std::string& line, const std::string& filename)
{
  std::stringstream parser(line);
  auto word = std::string();
  auto list = std::unordered_set<std::string>{ filename };
  auto counter = 0;

  while (std::getline(parser, word, ' '))
  {
    if (word.empty())
      continue;

    auto elem = this->wordInformation.insert(std::make_pair(word, list));

    if (not elem.second)
    {
      if (elem.first->second.insert(filename).second)
        counter++;
    }
    else
      counter++;
  }

  return counter;
}
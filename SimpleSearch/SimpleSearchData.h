#pragma once

#include <string>
#include <unordered_map>
#include <unordered_set>

class SimpleSearchData
{
  std::unordered_map<std::string, std::unordered_set<std::string>> wordInformation;
  std::unordered_map<std::string, int> nbWordByFile;

public:
  bool saveFile(const std::string& path);
  int howManyWordsInFile(const std::string& file) const;
  std::vector<std::string> getListOfFileThatIncludeWord(const std::string& word) const;

private:
  bool isCorrectFilePath(const std::string& path) const;
  int saveLine(const std::string& line, const std::string& filename);
};
#include <filesystem>
#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <iso646.h>
#include <iostream>

#include "SimpleSearchSolver.h"

SimpleSearchSolver::SimpleSearchSolver(const std::string& dir)
{
  for (auto& filePath : std::filesystem::directory_iterator(dir))
  {
    this->simpleSearchData.saveFile(filePath.path().string());
  }
}

// We only take unique word in input as we count unique word in file
// So we maintain a set to be sure to not count two times a word
// After that we create a map of file in key and number of words that we find
void SimpleSearchSolver::stats(const std::string& input) const
{
  std::stringstream parser(input);
  auto word = std::string();
  auto nbWordByFile = std::unordered_map<std::string, int>();
  auto alreadySeenWord = std::unordered_set<std::string>();
  auto nbWord = 0;

  while (std::getline(parser, word, ' '))
  {
    if (word.empty())
      continue;

    auto elem = alreadySeenWord.insert(word);

    if (elem.second)
    {
      nbWord++;
      auto listFile = this->simpleSearchData.getListOfFileThatIncludeWord(word);
      this->mapFileList(listFile, nbWordByFile);
    }
  }

  this->createTopList(nbWordByFile, nbWord);
}

void SimpleSearchSolver::mapFileList(const std::vector<std::string>& fileList, std::unordered_map<std::string, int>& nbWordByFile) const
{
  for (const auto& file : fileList)
  {
    auto elem = nbWordByFile.insert(std::make_pair(file, 1));

    if (not elem.second)
      elem.first->second++;
  }
}

// For our pourcentage calcul we simply took:
//  * number of unique input word find in file (nbWordFindInFile)
//  * number of unique word in input (nbUniqueWordInput)
// Pourcentage = 100 * nbWordFindInFile / nbUniqueWordInput
void SimpleSearchSolver::createTopList(const std::unordered_map<std::string, int>& nbWordByFile, int nbWord) const
{
  std::priority_queue<std::pair<std::string, int>, std::vector<std::pair<std::string, int>>, Compare> topList;

  for (const auto& elem : nbWordByFile)
  {
    auto totalWordInFile = this->simpleSearchData.howManyWordsInFile(elem.first);
    auto pourcentage = 100 * elem.second / nbWord;

    if (pourcentage > 0)
      topList.push(std::make_pair(elem.first, pourcentage));
  }

  this->printTopList(topList);
}

// A simple browsing priority_queue which stop at maximum 10 elements display
void SimpleSearchSolver::printTopList(std::priority_queue<std::pair<std::string, int>, std::vector<std::pair<std::string, int>>, Compare>& topList) const
{
  auto counter = 0;

  if (topList.empty())
  {
    std::cout << "no matches found" << std::endl;
    return;
  }

  while (not topList.empty())
  {
    auto elem = topList.top();
    topList.pop();

    std::cout << elem.first << ":" << elem.second << "%" << std::endl;

    counter++;

    if (counter > 9)
      break;
  }
}

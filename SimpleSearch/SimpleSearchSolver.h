#pragma once

#include <string>
#include <queue>

#include "SimpleSearchData.h"

class SimpleSearchSolver
{
  // This is our compare operator for our custom priority_queue
  struct Compare
  {
    bool operator()(const std::pair<std::string, int>& first, const std::pair<std::string, int>& second)
    {
      return first.second < second.second;
    }
  };


  SimpleSearchData simpleSearchData;

public:
  SimpleSearchSolver(const std::string& directory);
  void stats(const std::string& input) const;

private:
  void mapFileList(const std::vector<std::string>& fileList, std::unordered_map<std::string, int>& nbWordByFile) const;
  void createTopList(const std::unordered_map<std::string, int>& nbWordByFile, int nbWord) const;
  void printTopList(std::priority_queue<std::pair<std::string, int>, std::vector<std::pair<std::string, int>>, Compare>& topList) const;
};

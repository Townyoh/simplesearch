#include <filesystem>
#include <iso646.h>
#include <iostream>

#include "SimpleSearchSolver.h"

bool inputValidator(int argc, char **argv)
{
  if (argc != 2 || not std::filesystem::exists(argv[1]))
    return false;

  return true;
}

int main(int argc, char **argv)
{
  if (not inputValidator(argc, argv))
    std::cerr << "Program's argument error" << std::endl;

  try
  {
    auto dir = std::string(std::string(argv[1]));
    SimpleSearchSolver solver(dir);

    auto input = std::string();
    do
    {
      
      if (input == "!quit")
        break;

      if (not input.empty())
        solver.stats(input);

      std::cout << "search> ";
    } while (std::getline(std::cin, input));
  }
  catch (const std::string& e)
  {
    std::cerr << e << std::endl;
  }
  catch (...)
  {
    std::cerr << "An indefine error occurs" << std::endl;
  }

  return 0;
}
#include <iso646.h>

#include "pch.h"

#include "../SimpleSearch/ExtractData.h"

TEST(ExtractData_UnitTest, CorrectFileFirst)
{
  ExtractData extractor("..\\..\\TestFile\\DirectoryTestOne\\First.txt");
  auto line = std::string();
  auto data = std::vector<std::string>{"a b c", "d", "e f"};
  auto index = 0;

  while (extractor.getNextInformation(line))
  {
    EXPECT_EQ(line, data[index++]);
  }
}

TEST(ExtractData_UnitTest, CorrectFileSecond)
{
  ExtractData extractor("..\\..\\TestFile\\DirectoryTestOne\\Second.txt");
  auto line = std::string();
  auto data = std::vector<std::string>{ "a b", "c" };
  auto index = 0;

  while (extractor.getNextInformation(line))
  {
    if (!line.empty())
     EXPECT_EQ(line, data[index++]);
  }
}

TEST(ExtractData_UnitTest, UnableOpenFile)
{
  ASSERT_ANY_THROW(ExtractData extractor("..\\..\\TestFile\\DirectoryTestOne\\NoFile.txt"));
}

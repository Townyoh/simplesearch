#include "pch.h"
#include "../SimpleSearch/SimpleSearchData.h"

TEST(SimpleSearchData_UnitTest, FirstTextFile)
{
  auto filePath = std::string("..\\..\\TestFile\\DirectoryTestOne\\First.txt");
  auto filename = std::string("First.txt");
  SimpleSearchData searchData;
  searchData.saveFile(filePath);

  auto listFile = std::vector<std::string>{ filename };

  EXPECT_EQ(searchData.howManyWordsInFile(filename), 6);
  EXPECT_EQ(searchData.getListOfFileThatIncludeWord("a"), listFile);
}

TEST(SimpleSearchData_UnitTest, MultipleSameWordTextFile)
{
  auto filePath = std::string("..\\..\\TestFile\\DirectoryTestOne\\MultipleSameWord.txt");
  auto filename = std::string("MultipleSameWord.txt");
  SimpleSearchData searchData;
  searchData.saveFile(filePath);

  auto listFile = std::vector<std::string>{ filename };

  EXPECT_EQ(searchData.howManyWordsInFile(filename), 4);
  EXPECT_EQ(searchData.getListOfFileThatIncludeWord("a"), listFile);
}

TEST(SimpleSearchData_UnitTest, ManyTextFile)
{
  auto filePath1 = std::string("..\\..\\TestFile\\DirectoryTestOne\\First.txt");
  auto filename1 = std::string("First.txt");
  auto filePath2 = std::string("..\\..\\TestFile\\DirectoryTestOne\\Second.txt");
  auto filename2 = std::string("Second.txt");
  auto filePath3 = std::string("..\\..\\TestFile\\DirectoryTestOne\\MultipleSameWord.txt");
  auto filename3 = std::string("MultipleSameWord.txt");

  SimpleSearchData searchData;
  searchData.saveFile(filePath1);
  searchData.saveFile(filePath2);
  searchData.saveFile(filePath3);

  {
    EXPECT_EQ(searchData.howManyWordsInFile(filename1), 6);
    EXPECT_EQ(searchData.howManyWordsInFile(filename2), 3);
    EXPECT_EQ(searchData.howManyWordsInFile(filename3), 4);
  }

  {
    auto listFile = std::vector<std::string>{ filename1, filename2, filename3 };
    EXPECT_EQ(searchData.getListOfFileThatIncludeWord("a"), listFile);
  }

  {
    auto listFile = std::vector<std::string>{ filename1, filename3 };
    EXPECT_EQ(searchData.getListOfFileThatIncludeWord("d"), listFile);
  }

  {
    auto listFile = std::vector<std::string>{ filename1 };
    EXPECT_EQ(searchData.getListOfFileThatIncludeWord("f"), listFile);
  }

  {
    auto listFile = std::vector<std::string>{ };
    EXPECT_EQ(searchData.getListOfFileThatIncludeWord("z"), listFile);
  }
}

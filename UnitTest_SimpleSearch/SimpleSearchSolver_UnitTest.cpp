#include "pch.h"
#include "../SimpleSearch/SimpleSearchSolver.h"

#include <sstream>

TEST(SimpleSearchSolver_UnitTest, MultiplePourcentage)
{
  auto dir = std::string("..\\..\\TestFile\\DirectoryTestOne");
  SimpleSearchSolver solver(dir);

  std::stringstream ss;
  std::cout.rdbuf(ss.rdbuf());

  solver.stats("a d f e");

  auto result = std::vector<std::string>
  {
    "First.txt:100%",
    "MultipleSameWord.txt:50%",
    "Second.txt:25%"
  };

  auto file = std::string();
  auto counter = 0;
  while (std::getline(ss, file, '\n'))
  {
    if (!file.empty())
      EXPECT_EQ(file, result[counter++]);
  }
}

TEST(SimpleSearchSolver_UnitTest, NoWordFindInFiles)
{
  auto dir = std::string("..\\..\\TestFile\\DirectoryTestOne");
  SimpleSearchSolver solver(dir);

  std::stringstream ss;
  std::cout.rdbuf(ss.rdbuf());

  solver.stats("z q r");

  auto result = std::string
  {
    "no matches found\n"
  };

  EXPECT_EQ(ss.str(), result);
}

TEST(SimpleSearchSolver_UnitTest, MaximumTenResults)
{
  auto dir = std::string("..\\..\\TestFile\\DirectoryTestTwo");
  SimpleSearchSolver solver(dir);

  std::stringstream ss;
  std::cout.rdbuf(ss.rdbuf());

  solver.stats("a");

  auto file = std::string();
  auto counter = 0;
  while (std::getline(ss, file, '\n'))
  {
    counter++;
  }

  EXPECT_EQ(counter, 10);
}

TEST(SimpleSearchSolver_UnitTest, MultiplePourcentageV2)
{
  auto dir = std::string("..\\..\\TestFile\\DirectoryTestOne");
  SimpleSearchSolver solver(dir);

  std::stringstream ss;
  std::cout.rdbuf(ss.rdbuf());

  solver.stats("d f e");

  auto result = std::vector<std::string>
  {
    "First.txt:100%",
    "MultipleSameWord.txt:33%",
  };

  auto file = std::string();
  auto counter = 0;
  while (std::getline(ss, file, '\n'))
  {
    if (!file.empty())
      EXPECT_EQ(file, result[counter++]);
  }
}
